# WP-ID.org v3

https://wp-id.org

## Prerequisites

Make sure you have [Docker](https://docs.docker.com/engine/installation/) and [Docker Compose](https://docs.docker.com/compose/) and that you're able to run `docker run hello-world`:

## Usage

If you want to contribute (thank you!):

- Clone this repo and enter the directory:
  ```sh
  git clone https://gitlab.com/wp-id/website/wp-id.org.git wp-id.org
  cd wp-id.org
  ```
- Add `127.0.0.1 local.wp-id.org` to `/etc/hosts`
- Run the setup script:
  ```sh
  ./bin/setup-local
  ```
- Run the containers:
  ```sh
  docker compose up
  ```
- To bring down the containers:
  ```sh
  docker compose down
  ```
- To run WP CLI command:
  ```sh
  docker compose exec -u www-data backend wp <command>
  ```
- To import database dump:
  ```sh
  docker compose exec -u www-data -T backend wp db import - < PATH_TO_SQL_DUMP_FILE
  ```
- To export database:
  ```sh
  docker compose exec -u www-data -T backend wp db export - > PATH_TO_SQL_DUMP_FILE
  ```
