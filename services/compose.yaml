---
# Base compose file for all services on all environments.

version: "2.7"

networks:
  default:
    name: "${COMPOSE_PROJECT_NAME}-${WP_ENVIRONMENT_TYPE:?}"
  proxy:
    external: true
    name: ${PROXY_NETWORK:?}

volumes:
  db:
    name: "${COMPOSE_PROJECT_NAME}-${WP_ENVIRONMENT_TYPE}-db"
  uploads:
    name: "${COMPOSE_PROJECT_NAME}-${WP_ENVIRONMENT_TYPE}-uploads"

services:
  db:
    container_name: "${COMPOSE_PROJECT_NAME}-${WP_ENVIRONMENT_TYPE}-db"
    image: mariadb:11
    restart: always
    environment:
      MYSQL_DATABASE: ${MYSQL_DATABASE:-wordpress}
      MYSQL_PASSWORD: ${MYSQL_PASSWORD:?}
      MYSQL_ROOT_PASSWORD: ${MYSQL_ROOT_PASSWORD:?}
      MYSQL_USER: ${MYSQL_USER:-wordpress}
    networks:
      - default
    volumes:
      - "db:/var/lib/mysql"

  backend:
    container_name: "${COMPOSE_PROJECT_NAME}-${WP_ENVIRONMENT_TYPE}-backend"
    pull_policy: build
    restart: always
    build:
      context: ../
      dockerfile: services/backend/Dockerfile
    depends_on:
      - db
    extra_hosts:
      - "${DOMAIN:?}:${PROXY_IP_ADDRESS:?}"
    environment:
      # General
      DOMAIN: ${DOMAIN:?}
      PGID: $PGID
      PUID: $PUID
      # Mail
      SMTP_HOST: $SMTP_HOST
      SMTP_PASS: $SMTP_PASS
      SMTP_PORT: $SMTP_PORT
      SMTP_USER: $SMTP_USER
      # WordPress
      DB_HOST: db
      DB_NAME: $MYSQL_DATABASE
      DB_USER: $MYSQL_USER
      DB_PASSWORD: $MYSQL_PASSWORD
      WP_CACHE_HOST: cache
      WP_CACHE_PORT: 6379
      WP_ENVIRONMENT_TYPE: ${WP_ENVIRONMENT_TYPE}
      WP_MEMORY_LIMIT: ${WP_MEMORY_LIMIT:-256MB}
      AUTH_KEY: ${AUTH_KEY:?}
      AUTH_SALT: ${AUTH_SALT:?}
      LOGGED_IN_KEY: ${LOGGED_IN_KEY:?}
      LOGGED_IN_SALT: ${LOGGED_IN_SALT:?}
      NONCE_KEY: ${NONCE_KEY:?}
      NONCE_SALT: ${NONCE_SALT:?}
      SECURE_AUTH_KEY: ${SECURE_AUTH_KEY:?}
      SECURE_AUTH_SALT: ${SECURE_AUTH_SALT:?}
    networks:
      - default

  web:
    container_name: "${COMPOSE_PROJECT_NAME}-${WP_ENVIRONMENT_TYPE}-web"
    restart: always
    build:
      context: ./web
    depends_on:
      - backend
    labels:
      - "traefik.enable=true"
      - "traefik.http.routers.${COMPOSE_PROJECT_NAME}-${WP_ENVIRONMENT_TYPE}.entrypoints=http"
      - "traefik.http.routers.${COMPOSE_PROJECT_NAME}-${WP_ENVIRONMENT_TYPE}.rule=Host(`${DOMAIN}`) || Host(`www.${DOMAIN}`)"
    networks:
      - default
      - proxy
    volumes_from:
      - backend

  cache:
    container_name: "${COMPOSE_PROJECT_NAME}-${WP_ENVIRONMENT_TYPE}-cache"
    command: redis-server --maxmemory 32mb --maxmemory-policy allkeys-lru
    image: redis:7-alpine
    restart: always
    networks:
      - default
